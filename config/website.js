const tailwind = require('../tailwind');

module.exports = {
  pathPrefix: '/', // Prefix for all links. If you deploy your site to example.com/portfolio your pathPrefix should be "/portfolio"

  siteTitle: 'Un Artisan Développeur', // Navigation and Site Title
  siteTitleAlt: 'Un Artisan Développeur', // Alternative Site title for SEO
  siteUrl: 'https://mistermeridj.fr', // Domain of your site. No trailing slash!
  siteLanguage: 'fr', // Language Tag on <html> element
  siteLogo: '/logos/logo-1024.png', // Used for SEO and manifest
  siteDescription: `Je suis un artisan développeur passionné de JavaScript 🤝. Agiliste convaincu, j'accompagne les entreprises de toute taille dans le développement de leurs outils digitaux. Ainsi, je prends plaisir à créer un code beau, efficient et maintenable.`,

  // Manifest and Progress color
  themeColor: tailwind.colors.orange,
  backgroundColor: tailwind.colors.blue,
};
